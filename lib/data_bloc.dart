// data_bloc.dart
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:healping_app_ayuda_comunitaria/data_post.dart';
import 'package:healping_app_ayuda_comunitaria/data_state.dart';

import 'data_repository.dart';

class DataBloc extends Bloc<DataPost, DataState> {
  final DataRepository dataRepository;

  DataBloc({required this.dataRepository}) : super(DataInitialState()) {
    on<FetchDataPost>((event, emit) async {
      emit(DataLoadingState()); // Emite un estado de carga (opcional)
      try {
        final data = await dataRepository.fetchData();
        emit(DataLoadedState( data)); // Emite el estado con los datos cargados
      } catch (error) {
        emit(DataErrorState(error.toString())); // Emite un estado de error
      }
    });
  }
}




