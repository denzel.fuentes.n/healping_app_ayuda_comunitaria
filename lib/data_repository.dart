// data_repository.dart
import 'package:healping_app_ayuda_comunitaria/models/post.dart';

import 'service/api_service.dart';

class DataRepository {
  final ApiService apiService;

  DataRepository({required this.apiService});

  Future<List<Post>> fetchData() async {
    return await apiService.fetchUsers();
  }
}
