// data_state.dart

import 'package:healping_app_ayuda_comunitaria/models/post.dart';

abstract class DataState {}

class DataInitialState extends DataState {}

class DataLoadingState extends DataState {}

class DataLoadedState extends DataState {
  final List<Post> posts;  // Suponiendo que estás cargando una lista de usuarios

  DataLoadedState(this.posts);
}

class DataErrorState extends DataState {
  final String errorMessage;

  DataErrorState(this.errorMessage);
}
