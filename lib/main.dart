import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:healping_app_ayuda_comunitaria/data_bloc.dart';
import 'package:healping_app_ayuda_comunitaria/data_repository.dart';
import 'package:healping_app_ayuda_comunitaria/firebase_options.dart';
import 'package:healping_app_ayuda_comunitaria/screens/home_screen.dart';
import 'package:healping_app_ayuda_comunitaria/service/api_service.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: 'healping',
    theme: ThemeData(useMaterial3: true),
    home: Principal(),
  ));
}

class Principal extends StatefulWidget {
  Principal({Key? key}) : super(key: key);

  @override
  _PrincipalState createState() => _PrincipalState();
}

class _PrincipalState extends State<Principal> {
  late DataBloc _dataBloc;

  @override
  void initState() {
    super.initState();
    _dataBloc =
        DataBloc(dataRepository: DataRepository(apiService: ApiService()));
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _dataBloc,
      child: Home(),
    );
  }

  @override
  void dispose() {
    _dataBloc
        .close(); // Es importante cerrar el BLoC cuando ya no lo necesites.
    super.dispose();
  }
}
