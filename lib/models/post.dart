import 'dart:typed_data';

class Post {
  final String id;
  final String name;
  final String description;
  final Uint8List? imageBytes;
  Post({required this.id, required this.name, required this.description,this.imageBytes});

  // Método de fábrica para crear una instancia de Event desde un Map
  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
      id: json['_id'], // Nota que se utiliza '_id' para mapear el id
      name: json['name'],
      description: json['description'],
    );
  }
  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'description': description,
      'image':imageBytes
    };
  }
}
