import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:healping_app_ayuda_comunitaria/data_bloc.dart';
import 'package:healping_app_ayuda_comunitaria/data_post.dart';
import 'package:healping_app_ayuda_comunitaria/data_state.dart';
import 'package:healping_app_ayuda_comunitaria/screens/addpost_screen.dart';
import 'package:healping_app_ayuda_comunitaria/screens/login_screen.dart';
import 'package:healping_app_ayuda_comunitaria/screens/signup_screen.dart';
import 'package:healping_app_ayuda_comunitaria/screens/userprofile_screen.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  void verifylogin(BuildContext context, Widget widget) {
    if (_auth.currentUser?.email == null) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => LoginScreen(
                    message: "Necesitas Crearte Una cuenta",
                  )));
    } else {
      Navigator.push(context, MaterialPageRoute(builder: (context) => widget));
    }
  }

  @override
  Widget build(BuildContext context) {
    final dataBloc = BlocProvider.of<DataBloc>(context);

    dataBloc.add(FetchDataPost());
    String? email = _auth.currentUser?.email;
    return Container(
      margin: EdgeInsets.only(top: 25),
      child: Scaffold(
        appBar: AppBar(
          title: Container(
            padding: EdgeInsets.only(top: 25, bottom: 45),
            alignment: Alignment.centerLeft,
            child: Image.asset(
              'lib/assets/logo.jpg',
              width: 200,
              height: 200,
            ),
          ),
          actions: [
            Container(
              width: 50,
              height: 50,
              margin: EdgeInsets.only(right: 30, bottom: 20),
              child: Icon(
                Icons.menu,
                size: 35,
              ),
            )
          ],
          bottom: PreferredSize(
              preferredSize: Size.fromHeight(48),
              child: Row(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width * 0.92,
                    height: 53,
                    margin: EdgeInsets.only(left: 15),
                    decoration: ShapeDecoration(
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                        side: BorderSide(width: 1.50, color: Color(0xFF00830D)),
                        borderRadius: BorderRadius.circular(50),
                      ),
                    ),
                    child: Row(
                      children: [
                        Container(
                          width: 39,
                          height: 39,
                          margin: EdgeInsets.only(left: 12),
                          decoration: ShapeDecoration(
                            color: Color(0xFF84EB84),
                            shape: OvalBorder(
                              side: BorderSide(
                                  width: 1, color: Color(0xFF00AA00)),
                            ),
                          ),
                          child: Icon(Icons.search),
                        ),
                        InkWell(
                          onTap: () {
                            verifylogin(context, AddPostScreen());
                          },
                          child: Container(
                            width: 39,
                            height: 39,
                            margin: EdgeInsets.only(left: 12),
                            decoration: ShapeDecoration(
                              color: Color(0xFF84EB84),
                              shape: OvalBorder(
                                side: BorderSide(
                                    width: 1, color: Color(0xFF00AA00)),
                              ),
                            ),
                            child: Icon(Icons.add),
                          ),
                        ),
                        Spacer(),
                        InkWell(
                          onTap: () {
                            verifylogin(
                                context, UserProfileScreen(userEmail: email));
                          },
                          child: Container(
                            width: 39,
                            height: 39,
                            margin: EdgeInsets.only(right: 12),
                            decoration: ShapeDecoration(
                              color: Color(0xFF84EB84),
                              shape: OvalBorder(
                                side: BorderSide(
                                    width: 1, color: Color(0xFF00AA00)),
                              ),
                            ),
                            child: Icon(Icons.person),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              )),
        ),
        body: BlocBuilder<DataBloc, DataState>(
          builder: (context, state) {
            if (state is DataLoadingState) {
              return Center(child: CircularProgressIndicator());
            } else if (state is DataLoadedState) {
              return ListView.builder(
                itemCount: state.posts.length,
                itemBuilder: (context, index) => Card(
                    margin: EdgeInsets.only(top: 20),
                    child: Container(
                      margin: EdgeInsets.only(left: 15, top: 15, bottom: 15),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            state.posts[index].name,
                            style: TextStyle(
                                color: Colors.red, fontWeight: FontWeight.w500),
                          ),
                          Text(state.posts[index].description)
                        ],
                      ),
                    )),
              );
            } else if (state is DataErrorState) {
              return Center(child: Text('Error: ${state.errorMessage}'));
            } else {
              return Center(child: Text('No data available'));
            }
          },
        ),
      ),
    );
  }
}
