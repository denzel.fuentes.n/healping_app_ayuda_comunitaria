import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:healping_app_ayuda_comunitaria/data_bloc.dart';
import 'package:healping_app_ayuda_comunitaria/data_repository.dart';
import 'package:healping_app_ayuda_comunitaria/screens/home_screen.dart';
import 'package:healping_app_ayuda_comunitaria/service/api_service.dart';

class UserProfileScreen extends StatelessWidget {
  // Para este ejemplo, asumimos que el correo electrónico se pasa al constructor de la pantalla.
  // En una aplicación real, probablemente obtendrías esta información desde algún estado o servicio.
  final String? userEmail;

  UserProfileScreen({required this.userEmail});

  @override
  Widget build(BuildContext context) {
    final FirebaseAuth _auth = FirebaseAuth.instance;
    return Scaffold(
      backgroundColor: Color(0xFFE8F5E9), // Un tono verde claro para el fondo.
      appBar: AppBar(
        backgroundColor:
            Color(0xFF4CAF50), // Un verde más fuerte para la AppBar.
        title: Text("Perfil de Usuario"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircleAvatar(
              radius: 50,
              backgroundColor:
                  Color(0xFFA5D6A7), // Un verde medio para el avatar.
              child: Icon(Icons.person,
                  size: 50,
                  color: Color(
                      0xFF388E3C)), // Icono de persona en un verde más oscuro.
            ),
            SizedBox(height: 20),
            Text(
              userEmail!,
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Color(0xFF388E3C), // Texto en verde oscuro.
              ),
            ),
            SizedBox(height: 20),
            ElevatedButton(
              onPressed: () {
                _auth.signOut();
                Navigator.pop(context);
              },
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(
                    Color(0xFF4CAF50)), // Botón en verde.
                padding: MaterialStateProperty.all(
                    EdgeInsets.symmetric(horizontal: 30, vertical: 15)),
              ),
              child: Text("Logout"),
            ),
          ],
        ),
      ),
    );
  }
}
