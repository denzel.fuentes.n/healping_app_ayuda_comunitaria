// api_service.dart
import 'dart:convert';
import 'package:http/http.dart' as http;
import '../models/post.dart';

class ApiService {
  final String _baseUrl = "https://eventbloid.onrender.com/api";

  Future<List<Post>> fetchUsers() async {
    final response = await http.get(Uri.parse("$_baseUrl/event/all"));

    if (response.statusCode == 200) {
      List jsonResponse = json.decode(response.body)['data'];
      return jsonResponse.map((post) => Post.fromJson(post)).toList();
    } else {
      throw Exception('Failed to load post');
    }
  }

  Future<void> createPost(Post post) async {
    final Map<String, dynamic> postData = post.toJson();

    final response = await http.post(
      Uri.parse("$_baseUrl/event/"),
      headers: <String, String>{
        'Content-Type': 'application/json',
      },
      body: jsonEncode(postData),
    );

    if (response.statusCode == 200) {
      print('Contenido registrado exitosamente.');
    } else {
      // Maneja errores, por ejemplo:
      throw Exception('Error al registrar contenido en la API');
    }
  }
}
